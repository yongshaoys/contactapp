//
//  contactDetailViewController.swift
//  contactApp
//
//  Created by 邵勇 on 4/10/18.
//  Copyright © 2018 Yong Shao. All rights reserved.
//

import UIKit

class ContactDetailViewController: UIViewController {
    
    @IBOutlet weak var contactImg: UIImageView!
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtBirthD: UITextField!
    @IBOutlet weak var txtAddress: UITextField!
    
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnEditOrSubmit: UIButton!
    
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    
    
    var contactObj : Contact?
    var createNewContact = false
    var originalConstriant : CGFloat = 0
    
    
    let imagePickerController = UIImagePickerController()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        originalConstriant = topConstraint.constant
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)

        imagePickerController.delegate = self
        imagePickerController.allowsEditing = true
        
        contactImg.layer.cornerRadius = contactImg.frame.size.height/2
        contactImg.layer.borderWidth = 1.0
        contactImg.layer.borderColor = UIColor.black.cgColor
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        contactImg.isUserInteractionEnabled = true
        contactImg.addGestureRecognizer(tapGestureRecognizer)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        btnSave.isHidden = true
        btnCancel.isHidden = true
        
        if createNewContact {
            btnEditOrSubmit.isSelected = true
        } else {
            showContactDetail()
        }
        
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        if imagePickerController.sourceType == .photoLibrary {
            imagePickerController.sourceType = .photoLibrary
        }
        present(imagePickerController, animated: true, completion: nil)
    }
    
    
    
    func showContactDetail(){
        btnEditOrSubmit.isSelected = false
        
        txtEmail.text = contactObj?.email
        txtPhone.text = contactObj?.mobileNumber
        txtBirthD.text = contactObj?.birthDate
        txtLastName.text = contactObj?.lastName
        txtFirstName.text = contactObj?.firstName
        txtAddress.text = contactObj?.address
        
        
        let docUrl = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
        if let imgUrl = contactObj?.photoUrl{
            let file_url = docUrl?.appendingPathComponent("\(imgUrl).jpeg")
            print(file_url)
            if let img_data = try? Data(contentsOf: file_url!) {
                contactImg.image = UIImage(data: img_data)
            }
        } else {
            contactImg.image = nil
        }
    }
    
    
    @IBAction func btnCancel(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    
    
    @IBAction func btnSave(_ sender: UIButton) {
        
        var imgUrl : String = ""
        guard let firstName = txtFirstName.text, let lastName = txtLastName.text, let mobileNumber = txtPhone.text, let email = txtEmail.text, let birthDate = txtBirthD.text, let address = txtAddress.text else{return}
        
        if let img = contactImg.image {
            if let data = UIImageJPEGRepresentation(img, 0.8) {
                let docUrl = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
                imgUrl = UUID().uuidString
                let file_url = docUrl?.appendingPathComponent("\(imgUrl).jpeg")
                print(file_url)
                try? data.write(to: file_url!)
            }
        }
        
        CoreDataManager.sharedManager.updateContact(firstName: firstName, lastName: lastName, mobileNumber: mobileNumber, email: email, birthDate: birthDate, address: address, photoUrl: imgUrl, contact: contactObj! )
        
        navigationController?.popViewController(animated: true)
    
    }
    
    
    @IBAction func btnEditOrSubmitAction(_ sender: UIButton) {
        
        if btnEditOrSubmit.isSelected == false {
            btnSave.isHidden = false
            btnCancel.isHidden = false
            btnEditOrSubmit.isHidden = true
        } else {
            var imgUrl : String = ""
            guard let firstName = txtFirstName.text, let lastName = txtLastName.text, let mobileNumber = txtPhone.text, let email = txtEmail.text, let birthDate = txtBirthD.text, let address = txtAddress.text else{return}
            
            if let img = contactImg.image {
                if let data = UIImageJPEGRepresentation(img, 0.8) {
                    let docUrl = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
                    imgUrl = UUID().uuidString
                    let file_url = docUrl?.appendingPathComponent("\(imgUrl).jpeg")
                    print(file_url)
                    try? data.write(to: file_url!)
                }
            }
            CoreDataManager.sharedManager.insertContact(firstName: firstName, lastName: lastName, mobileNumber: mobileNumber, email: email, birthDate: birthDate, address: address, photoUrl: imgUrl)
            
            navigationController?.popViewController(animated: true)
            
            
        }

    }
    
    
    @objc func keyboardWillShow(notification: Notification) {
        let info: Dictionary = notification.userInfo!
        let valueOfKeyboardSizePosition: NSValue = info[UIKeyboardFrameEndUserInfoKey] as! NSValue
        let keyboardSize = valueOfKeyboardSizePosition.cgRectValue
        let height = keyboardSize.height
        print(height)
        let valueOfKeybaordAnimation: NSValue = info[UIKeyboardAnimationDurationUserInfoKey] as! NSValue
        var time: TimeInterval = 0
        valueOfKeybaordAnimation.getValue(&time)
        topConstraint.constant = topConstraint.constant - height
        UIView.animate(withDuration: time) { () -> Void in
            self.view.layoutIfNeeded()
        }
    }
    
    @objc func keyboardWillHide(notification: Notification) {
        topConstraint.constant = originalConstriant
        
        let info: Dictionary = notification.userInfo!
        let valueOfKeybaordAnimation: NSValue = info[UIKeyboardAnimationDurationUserInfoKey] as! NSValue
        var time: TimeInterval = 0
        valueOfKeybaordAnimation.getValue(&time)
        
        UIView.animate(withDuration: time) { () -> Void in
            self.view.layoutIfNeeded()
        }
        
    }
    
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    

    
}


extension ContactDetailViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            contactImg.image = pickedImage
        }
        dismiss(animated: true, completion: nil)
    }
}

extension ContactDetailViewController: UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField != txtFirstName {
            NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        }
    
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
    }
    
    
}








