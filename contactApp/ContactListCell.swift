//
//  contactListCell.swift
//  contactApp
//
//  Created by 邵勇 on 4/10/18.
//  Copyright © 2018 Yong Shao. All rights reserved.
//

import UIKit

class ContactListCell: UITableViewCell {
    
    @IBOutlet weak var contactImg: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    
    
    
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        
        contactImg.layer.cornerRadius = contactImg.frame.size.height/2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
