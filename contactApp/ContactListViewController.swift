//
//  ViewController.swift
//  contactApp
//
//  Created by 邵勇 on 4/10/18.
//  Copyright © 2018 Yong Shao. All rights reserved.
//

import UIKit
import MessageUI


class ContactListViewController: UIViewController {

    @IBOutlet weak var tblContactList: UITableView!
    
    var contactArr : [Contact] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        contactArr = []
        getContactList()
        tblContactList.reloadData()
    }
    
    func getContactList1 (){
        CoreDataManager.sharedManager.fetchAllEntities(entityName: "Contact") { (arrContact) in
            for item in arrContact!{
                let mmo = CoreDataManager.sharedManager.persistentContainer.viewContext.object(with: item.objectID)
                self.contactArr.append(mmo as! Contact)
            }
        }
    }
    
    func getContactList (){
        if let contacts = CoreDataManager.sharedManager.fetchAllContacts(){
            contactArr = contacts
        }
    }
    
    
    @IBAction func btnAddContact(_ sender: UIButton) {
        let controller = storyboard?.instantiateViewController(withIdentifier: "ContactDetailViewController") as! ContactDetailViewController
        controller.createNewContact = true
    
        navigationController?.pushViewController(controller, animated: true)
    }
    
    
    @IBAction func btnDeleteContact(_ sender: UIButton) {
        
        
        
    }
    
    
    
    func sendEmail(emailAddress: String) {
        print("sending email")
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients([emailAddress])
            mail.setMessageBody("<p>You're so awesome!</p>", isHTML: true)
            
            present(mail, animated: true)
        } else {
            print("MFMailComposeViewController.canSendMail() is false")

        }
    }
    
    
    func callContact(phoneNumber: String){
        print("test call")
        
        let phoneString = "TEL://1" + phoneNumber
        
        let url: NSURL = URL(string: phoneString)! as NSURL
        UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)

    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }


}




extension ContactListViewController: MFMailComposeViewControllerDelegate{
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: {() in
            
        })
    }
}





extension ContactListViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contactArr.count
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblContactList.dequeueReusableCell(withIdentifier: "ContactListCell", for: indexPath) as! ContactListCell
        cell.lblName.text = contactArr[indexPath.row].firstName! + " " +  contactArr[indexPath.row].lastName!
        cell.lblPhone.text = contactArr[indexPath.row].mobileNumber
        cell.lblEmail.text = contactArr[indexPath.row].email
        
        
        let docUrl = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
        if let imgUrl = contactArr[indexPath.row].photoUrl{
            let file_url = docUrl?.appendingPathComponent("\(imgUrl).jpeg")
            print(file_url)
            if let img_data = try? Data(contentsOf: file_url!) {
                cell.contactImg.image = UIImage(data: img_data)
            }
        } else {
            cell.contactImg.image = nil
        }
        
        return cell
    }
    
    
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    
    
    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let alert = UIAlertController(title: nil, message: "Please select your action", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Show Contact Detail", style: .default, handler: { _ in
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "ContactDetailViewController") as! ContactDetailViewController
            controller.contactObj = self.contactArr[indexPath.row]
            controller.createNewContact = false
            self.navigationController?.pushViewController(controller, animated: true)
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        alert.addAction(UIAlertAction(title: "Send Email", style: .default, handler: {_ in
            self.sendEmail(emailAddress: self.contactArr[indexPath.row].email!)
        }))
        
        alert.addAction(UIAlertAction(title: "Call", style: .default, handler: {_ in
            self.callContact(phoneNumber: self.contactArr[indexPath.row].mobileNumber!)
        }))
        
        
        present(alert, animated: true, completion: nil)
        
        
//        1-    Show Details: push the second view (DetailsView) that has form to show all the details of the contact.

    }
    
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath)
        -> [UITableViewRowAction]? {
            
            let more = UITableViewRowAction(style: .normal, title: "More") {
                action, index in
            }
            more.backgroundColor = UIColor.lightGray
            
            let favorite = UITableViewRowAction(style: .normal, title: "Flag") {
                action, index in
            }
            favorite.backgroundColor = UIColor.orange
            
            let delete = UITableViewRowAction(style: .normal, title: "Delete") {
                action, index in
                
                CoreDataManager.sharedManager.deleteContact(contact: self.contactArr[indexPath.row])
                self.contactArr.remove(at: indexPath.row)
                tableView.deleteRows(at: [indexPath], with: .automatic)
                
            }
            delete.backgroundColor = UIColor.red
            
            return [delete, favorite, more]
    }
    
    
    
}







/*
 
 Task:
 
 Core data
 
 Create an Application that manages contacts. All the data will be saved locally on the device.
 
 We need to create 2 views (ListView & DetailsView). Also we need to create a model class (DataManager) that can do all operations (Add, Edit, Delete) and notify the view using handlers.
 
 1- ListView:
 
 - It has a ListView to show the list of my contacts. It will show the Name, Mobile Number, Email address, Photo for each contact. When I tap on one item, you show an action sheet with the following options:
 1-    Show Details: push the second view (DetailsView) that has form to show all the details of the contact.
 2-    Call: show standard call popview.
 3-    Send an Email: show standard mail composer controller.
 - Show ActivityIndicator View during loading the list.
 - Initiate data loading using the DataManger and provide the handler for it to be got invoked once data finished load.
 - Dismiss ActivityIndicator View.
 - Bind the data to ListView cells.
 - It has “Add” button on the right top corner of the view. Click on Add button will push to second view (DetailsView) to add new contact.
 - It has “Delete” button when the user swipe any item as standard behavior to allow the user to delete the selected contact in case and show proper message to the user in both cases (successful or failure).
 
 - DetailsView
 - It has a form that shows all the information of the contact. The form of contact details will include the following fields:
 1-    First Name: Alphabetical field and it is mandatory.
 2-    Last Name: Alphabetical field and it is mandatory.
 3-    Mobile Number: Numeric field and it is mandatory.
 4-    Email Address: it is mandatory.
 5-    Birth Date: Date field and it is not mandatory.
 6-    Address: text field and it is not mandatory.
 7-    Photo: it is not mandatory.
 
 You have to consider standard validation for each field.
 
 - It has Edit button on the right top corner of the view in case the user needs to edit the details of the contact. When the user click on this button will show “Cancel” & “Save” buttons on the top left and right corners either to cancel the operation or to save the changes.
 
 */





