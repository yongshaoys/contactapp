//
//  CoreDataManager.swift
//  contactApp
//
//  Created by 邵勇 on 4/10/18.
//  Copyright © 2018 Yong Shao. All rights reserved.
//

import Foundation
import CoreData

typealias completion = ([NSManagedObject]?)->()

class CoreDataManager {
    static let sharedManager = CoreDataManager()
    private init(){}
    
    lazy var persistentContainer : NSPersistentContainer = {
        let container = NSPersistentContainer(name: "contactApp")
        container.loadPersistentStores(completionHandler: {(storeDescription, error) in
            if let error = error as NSError?{
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    
    func saveContext(){
        let context = CoreDataManager.sharedManager.persistentContainer.viewContext
        if context.hasChanges{
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    
    func fetchAllContacts() -> [Contact]? {
        
        let managedContext = CoreDataManager.sharedManager.persistentContainer.viewContext
        let pageFetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Contact")
        do {
            if let contacts = try managedContext.fetch(pageFetchRequest) as? [Contact]{
                return contacts
            } else {
                return nil
            }
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
            return nil
        }
    }
    
    
    func insertContact(firstName: String, lastName: String, mobileNumber: String, email: String, birthDate: String, address: String, photoUrl: String) {
        let managedContext = CoreDataManager.sharedManager.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "Contact", in: managedContext)
        let contact = NSManagedObject(entity: entity!, insertInto: managedContext) as! Contact
        contact.firstName = firstName
        contact.lastName = lastName
        contact.mobileNumber = mobileNumber
        contact.email = email
        contact.birthDate = birthDate
        contact.address = address
        contact.photoUrl = photoUrl
        
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    
    func updateContact(firstName: String, lastName: String, mobileNumber: String, email: String, birthDate: String, address: String, photoUrl: String, contact: Contact){
        
        let managedContext = CoreDataManager.sharedManager.persistentContainer.viewContext
        
        contact.firstName = firstName
        contact.lastName = lastName
        contact.mobileNumber = mobileNumber
        contact.email = email
        contact.birthDate = birthDate
        contact.address = address
        contact.photoUrl = photoUrl
        
        do {
            try managedContext.save()
            print("saved!")
        } catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        }
    }
    
    
    func deleteContact (contact: Contact) {
        let managedContext = CoreDataManager.sharedManager.persistentContainer.viewContext
        managedContext.delete(contact)
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    
    
    
    func fetchAllEntities(entityName : String, completion : @escaping completion ){
        
        
        /*Before you can do anything with Core Data, you need a managed object context, and this time backgroundContext */
        let managedContext = CoreDataManager.sharedManager.persistentContainer.newBackgroundContext()
        
        managedContext.perform {
            
            /*As the name suggests, NSFetchRequest is the class responsible for fetching from Core Data.
             
             Initializing a fetch request with init(entityName:), fetches all objects of a particular entity. This is what you do here to fetch all Person entities.
             */
            let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: entityName)
            
            /*You hand the fetch request over to the managed object context to do the heavy lifting. fetch(_:) returns an array of managed objects meeting the criteria specified by the fetch request.*/
            do {
                let managedObjectArray = try managedContext.fetch(fetchRequest)
                completion(managedObjectArray)
            } catch let error as NSError {
                print("Could not fetch. \(error), \(error.userInfo)")
                completion(nil)
            }
        }
    }
    
    func fetchAllEntities1(entityName : String, completion : @escaping completion ){
        
        
        /*Before you can do anything with Core Data, you need a managed object context, lets use   */
        CoreDataManager.sharedManager.persistentContainer.performBackgroundTask { (managedContext) in
            
            
            
            /*As the name suggests, NSFetchRequest is the class responsible for fetching from Core Data.
             
             Initializing a fetch request with init(entityName:), fetches all objects of a particular entity. This is what you do here to fetch all Person entities.
             */
            let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: entityName)
            
            /*You hand the fetch request over to the managed object context to do the heavy lifting. fetch(_:) returns an array of managed objects meeting the criteria specified by the fetch request.*/
            do {
                let managedObjectArray = try managedContext.fetch(fetchRequest)
                completion(managedObjectArray)
            } catch let error as NSError {
                print("Could not fetch. \(error), \(error.userInfo)")
                completion(nil)
            }
        }
    }
    
    
    
    
    
}
