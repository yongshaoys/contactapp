//
//  Contact+CoreDataProperties.swift
//  contactApp
//
//  Created by 邵勇 on 4/10/18.
//  Copyright © 2018 Yong Shao. All rights reserved.
//
//

import Foundation
import CoreData


extension Contact {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Contact> {
        return NSFetchRequest<Contact>(entityName: "Contact")
    }

    @NSManaged public var address: String?
    @NSManaged public var birthDate: String?
    @NSManaged public var email: String?
    @NSManaged public var firstName: String?
    @NSManaged public var lastName: String?
    @NSManaged public var mobileNumber: String?
    @NSManaged public var photoUrl: String?

}
